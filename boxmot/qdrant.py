import logging
import uuid
from typing import List

import numpy as np
from qdrant_client import QdrantClient
from qdrant_client.http import models


class QdrantReId:
    def __init__(self, host: str = "localhost", port: int = 6333):
        self.host = host
        self.port = port
        self.client = QdrantClient(host, port=port)
        name_collection = "test-reid"
        check = True if name_collection in [item.name for item in self.client.get_collections().collections] else False
        if check == False:
            self.client.create_collection(
                collection_name=name_collection,
                vectors_config=models.VectorParams(size=512, distance=models.Distance.COSINE),
            )

    def generate_unique_id(self):
        return str(uuid.uuid4())

    def upsert(self, collection_name: str, list_data: List[dict], metadata):
        documents = []
        for embedded in list_data:
            document = {
                "id": self.generate_unique_id(),
                "vector": embedded,
                "payload": metadata,
            }

            documents.append(models.PointStruct(**document))

        self.client.upsert(collection_name=collection_name, points=documents)

    def update(self, collection_name, mode, point_ids: List, key_value: dict):
        if mode == "payload":
            self.client.set_payload(
                collection_name=f"{collection_name}",
                payload=key_value,
                points=point_ids,
            )

    def get_data(
        self,
        collection_name: str,
        **key_value: dict,
    ):
        if "all" in key_value.keys():
            return self.client.scroll(
                collection_name=f"{collection_name}", with_payload=True, with_vectors=True, limit=100000
            )
        else:
            return self.client.scroll(
                collection_name=f"{collection_name}",
                scroll_filter=models.Filter(
                    must=[
                        models.FieldCondition(
                            key=key,
                            match=models.MatchValue(value=value),
                        )
                        for key, value in zip(key_value.keys(), key_value.values())
                    ]
                ),
                with_payload=True,
                with_vectors=True,
                limit=100000,
            )

    def get_distance_type(self, collection_name: str) -> str:
        info_collection = [
            item for item in self.client.get_collection(collection_name=collection_name).config.params.vectors
        ]
        return info_collection[1][1].value

    def get_threshold(self, distance_type) -> float:
        if distance_type == "Cosine":
            return 0.7
        else:
            return 500

    def search(
        self,
        query_vector: List,
        collection_name: str = None,
    ):
        """
        The `search` function takes a query vector, collection name, and top-k value as input, performs
        a search using the query vector in the specified collection, and returns the name of the
        document with the highest distance score that exceeds the threshold.

        :param query_vector: The query_vector parameter is a list that represents the vectorized query
        for searching in the collection. It contains the numerical values that represent the features or
        attributes of the query
        :type query_vector: List
        :param collection_name: The `collection_name` parameter is the name of the collection in which
        you want to perform the search. A collection is a group of similar documents that are stored
        together for efficient searching and retrieval
        :type collection_name: str
        :param top_k: The `top_k` parameter specifies the maximum number of search results to return. It
        determines how many nearest neighbors to retrieve from the index
        :type top_k: int
        :return: the name of the document that matches the search query.
        """
        search_result = self.client.search(
            collection_name=collection_name, query_vector=query_vector, limit=3, with_payload=True
        )
        # name = None
        for result in search_result:
            document_id = result.id
            distance = result.score
            payload = result.payload
            distance_type = self.get_distance_type(collection_name)
            if distance >= self.get_threshold(distance_type):
                print(f"Document ID: {document_id}, Distance: {distance}, Payload: {payload}")
        #         name = payload["track_id"]
                return search_result
        return []
